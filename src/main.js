import "font-awesome/css/font-awesome.css";
import "vue-sidebar-menu/dist/vue-sidebar-menu.css";
import Vue from "vue";

import App from "./App";

import "./config/msgs";
import "./config/bootstrap";
import store from "./config/store";
import router from "./config/router";

import ModalPlugin from "bootstrap-vue";

Vue.use(ModalPlugin);

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
