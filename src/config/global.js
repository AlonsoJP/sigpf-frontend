//export const baseApiUrlResource = "http://sigpf.com.br:8090/api"; //usar em produção
//export const baseApiUrlAuth = "http://sigpf.com.br:8091"; //usar em produção
export const baseApiUrlResource = "http://localhost:8090/api"; //usar em dev
export const baseApiUrlAuth = "http://localhost:8091"; //usar em dev
export const userKey = "__aeu_token";
import Vue from "vue";

export function showError(status, causa) {
  const resposta = status + " " + causa;

  //switch (status) {
    //case 400:
     // Vue.toasted.global.error400(resposta);
     // break;
   // case 401:
    //  Vue.toasted.global.error400(resposta);
    //  break;
  //  case 500:
    //  Vue.toasted.global.error500(resposta);
    // break;
  //}
  Vue.toasted.global.base(resposta);
}

export default { baseApiUrlResource, baseApiUrlAuth, userKey, showError };
