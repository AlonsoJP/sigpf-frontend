import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import Vue from "vue";
import BootstrapVue from "bootstrap-vue";

import VBTogglePlugin from "bootstrap-vue";
import ModalPlugin from "bootstrap-vue";
import TablePlugin from "bootstrap-vue";

Vue.use(BootstrapVue);
Vue.use(TablePlugin);

Vue.use(VBTogglePlugin);
Vue.use(ModalPlugin);
