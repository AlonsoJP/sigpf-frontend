import VueCookie from "vue-cookie";
import axios from "axios";
import Vue from "vue";
import Vuex from "vuex";
import VueJwtDecode from "vue-jwt-decode";
import {
  baseApiUrlAuth,
  baseApiUrlResource,
  userKey,
  showError,
} from "./global";

Vue.use(Vuex);
Vue.use(VueCookie);
Vue.use(VueJwtDecode);

export default new Vuex.Store({
  state: {
    isMenuVisible: false,
    token: null,
    userData: null,
    provisorio: null,
  },
  mutations: {
    toggleMenu(state, payload) {
      if (!state.token) {
        state.isMenuVisible = false;
        return;
      }

      if (payload === undefined) {
        state.isMenuVisible = !state.isMenuVisible;
      } else {
        state.isMenuVisible = payload;
      }
    },
    SET_USER_DATA(state, payload) {
      state.userData = payload;
      if (payload) {
        state.isMenuVisible = false;
      } else {
        state.isMenuVisible = false;
      }
    },
    SET_TOKEN(state, payload) {
      state.token = null;

      delete axios.defaults.headers.common["Authorization"];
      state.token = payload;
      if (payload) {
        delete axios.defaults.headers.common["Authorization"];
        axios.defaults.headers.common["Authorization"] = `Bearer ${payload}`;
      } else {
        delete axios.defaults.headers.common["Authorization"];
      }
    },
    SET_PROVISORIO(state, payload) {
      state.provisorio = payload;
    },
  },
  actions: {
    refreshToken(context, payload) {
      var formData = new FormData();

      formData.append("refresh_token", VueCookie.get("refreshToken"));
      formData.append("grant_type", "refresh_token");

      axios
        .post(`${baseApiUrlAuth}/oauth/token`, formData, {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization: "Basic dnVlanM6dnVlanM=",
          },
        })
        .then((res) => {
          localStorage.setItem(userKey, JSON.stringify(res.data.access_token));
          context.commit("SET_TOKEN", res.data.access_token);
          context.dispatch("loadUser", res.data.access_token);
          delete axios.defaults.headers.common["Authorization"];
          axios.defaults.headers.common[
            "Authorization"
          ] = `Bearer ${res.data.access_token}`;
          
        })
        .catch((error) => {
          showError(
            error.response.status,
            error.response.data.error_description
          );
          console.log(JSON.stringify(error.response));
        });
    },
    validateToken(context, payload) {
      try {
        const jtoken = JSON.parse(localStorage.getItem(userKey));
        const xp = VueJwtDecode.decode(jtoken).exp;
        const now = new Date();

        now.getTime() > xp * 1000;
      } catch (error) {
        console.log(error);
      }
    },
    validateTokenRefresh(context, payload) {
      if (context.dispatch("validateToken")) {
        context.dispatch("refreshToken");
      } else {
        context.dispatch("refreshToken");
      }
    },
    loadUser(context, payload) {
      //delete axios.defaults.headers.common["Authorization"];
      //axios.defaults.headers.common["Authorization"] = `Bearer ${payload}`;
      axios
        .get(
          `${baseApiUrlResource}/users`,
          {
            //headers: {
            //Authorization: `Bearer ${payload}`,
            // },
          }
          /*config*/
        )
        .then((res) => {
          context.commit("SET_USER_DATA", res.data);
        })
        .catch((error) => {
          showError(
            error.response.status,
            error.response.data.error_description,
            console.log(error.response.status)
          );
        });
    },
  },
});
