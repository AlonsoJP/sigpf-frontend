import Vue from "vue";
import Toasted from "vue-toasted";

Vue.use(Toasted, {
  iconPack: "fontawesome",
  duration: 3000,
});

Vue.toasted.register(
  "defaultSuccess",
  (payload) => (!payload.msg ? "Operação realizada com sucesso!" : payload.msg),
  {
    type: "success",
    icon: "check",
  }
);

Vue.toasted.register(
  "defaultError",
  (payload) => (!payload.msg ? "Erro inesperado" : payload.msg),
  {
    type: "error",
    icon: "times",
  }
);

//padrao personalizado
Vue.toasted.register(
  "base",
  (payload) => (!payload.msg ? "Status Code Error: "+payload : payload.msg),
  {
    type: "error",
    icon: "times",
  }
);

//faixa 400
Vue.toasted.register(
    "error400",
    (payload) => (!payload.msg ? "Status Code Error: "+payload : payload.msg),
    {
      type: "error",
      icon: "times",
    }
  );

  Vue.toasted.register(
    "error404",
    (payload) => (!payload.msg ? "Status Code Error: "+payload : payload.msg),
    {
      type: "error",
      icon: "times",
    }
  );

  Vue.toasted.register(
    "error500",
    (payload) => (!payload.msg ? "Status Code Error: "+payload : payload.msg),
    {
      type: "error",
      icon: "times",
    }
  );