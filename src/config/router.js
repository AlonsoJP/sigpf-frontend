import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../components/home/Home";
import AdminPages from "../components/admin/Admin";
import Auth from "../components/auth/Auth";
import Association from "../components/association/Association";
import District from "../components/adress/District";
import Street from "../components/adress/Street";
import University from "../components/university/University";
import Course from "../components/university/Course";
import Vechile from "../components/company/Vechile";
import Company from "../components/company/Company";
import Driver from "../components/company/Driver";
import Recipe from "../components/finances/Recipe";
import Member from "../components/association/Member";
import Bank from "../components/finances/Bank";
import Preferences from "../components/preferences/Preferences";
import SupportCityHall from "../components/finances/SupportCityHall";
import Student from "../components/association/Student";
import Input from "../components/finances/Input";
import Output from "../components/finances/Output";
import History from "../components/finances/History";
import StudentData from "../components/statistics/StudentData";
import FinancesData from "../components/statistics/FinancesData";
import Estimates from "../components/statistics/Estimates";

Vue.use(VueRouter);

const routes = [
  {
    name: "Home",
    path: "/",
    component: Home,
  },
  { name: "AdminPages", path: "/admin", component: AdminPages },
  { name: "Auth", path: "/auth", component: Auth },
  { name: "Association", path: "/association", component: Association },
  { name: "District", path: "/district", component: District },
  { name: "Street", path: "/street", component: Street },
  { name: "University", path: "/university", component: University },
  { name: "Course", path: "/course", component: Course },
  { name: "Vechile", path: "/vechile", component: Vechile },
  { name: "Company", path: "/company", component: Company },
  { name: "Driver", path: "/driver", component: Driver },
  { name: "Recipe", path: "/recipe", component: Recipe },
  { name: "Member", path: "/member", component: Member },
  { name: "Bank", path: "/bank", component: Bank },
  { name: "Preferences", path: "/preferences", component: Preferences },
  { name: "SupportCityHall", path: "/support", component: SupportCityHall },
  { name: "Student", path: "/student", component: Student },
  { name: "Input", path: "/input", component: Input },
  { name: "Output", path: "/output", component: Output },
  { name: "History", path: "/history", component: History },
  { name: "StudentData", path: "/statistics/student", component: StudentData },
  {
    name: "FinancesData",
    path: "/statistics/finances",
    component: FinancesData,
  },
  { name: "Estimates", path: "/statistics/estimates", component: Estimates },
];

export default new VueRouter({
  mode: "history",
  routes,
});
